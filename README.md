# zuritest: evaluation framework for zurichess

zuritest is an evaluation framework for zurichess. It is largely
inspired from [fishtest](http://tests.stockfishchess.org/tests) which
is used to test, evaluate and tune Stockfish.

package common

import (
	"bytes"
	"net/http"

	"github.com/gorilla/rpc/v2/json"
)

// Create creates a new match.
type CreateRequest struct {
	Description string
	GamesToPlay int
	TimeControl TimeControl
	BaseEngine  Engine
	TestEngine  Engine
	SPRT        SPRT
}

type CreateResponse struct {
	Id string
}

// GetWork gets more work.
type GetWorkRequest struct {
}

type GetWorkResponse struct {
	Id          string
	GamesToPlay int
	TimeControl TimeControl
	BaseEngine  Engine
	TestEngine  Engine
}

// SubmitWork submits work.
type SubmitWorkRequest struct {
	Id    string
	Score Score
}

type SubmitWorkResponse struct {
}

type MatchServiceClient struct {
	Address string
}

// execute makes and RPC call.
func (msc *MatchServiceClient) execute(method string, req, resp interface{}) error {
	reqBody, err := json.EncodeClientRequest(method, req)
	if err != nil {
		return err
	}
	httpResp, err := http.Post(msc.Address, "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		return err
	}
	return json.DecodeClientResponse(httpResp.Body, resp)
}

func (msc *MatchServiceClient) Create(req *CreateRequest) (*CreateResponse, error) {
	resp := &CreateResponse{}
	if err := msc.execute("MatchService.Create", req, resp); err != nil {
		return nil, err
	}
	return resp, nil
}

func (msc *MatchServiceClient) GetWork(req *GetWorkRequest) (*GetWorkResponse, error) {
	resp := &GetWorkResponse{}
	if err := msc.execute("MatchService.GetWork", req, resp); err != nil {
		return nil, err
	}
	return resp, nil
}

func (msc *MatchServiceClient) SubmitWork(req *SubmitWorkRequest) (*SubmitWorkResponse, error) {
	resp := &SubmitWorkResponse{}
	if err := msc.execute("MatchService.SubmitWork", req, resp); err != nil {
		return nil, err
	}
	return resp, nil
}

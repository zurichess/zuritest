package common

import (
	"fmt"
	"strings"
	"time"
)

// TimeControl defines time control for chess games.
type TimeControl struct {
	Moves int
	Time  time.Duration
	Inc   time.Duration
}

// Scale returns a new time control scaled s times.
func (tc TimeControl) Scale(s float64) TimeControl {
	return TimeControl{
		Moves: tc.Moves,
		Time:  time.Duration(float64(tc.Time)*s + 0.5),
		Inc:   time.Duration(float64(tc.Inc)*s + 0.5),
	}
}

// String returns cutechess representation.
func (tc TimeControl) String() string {
	return fmt.Sprintf("%d/%.3f+%.3f", tc.Moves, tc.Time.Seconds(), tc.Inc.Seconds())
}

func (tc TimeControl) IsValid() bool {
	return tc.Moves > 0 &&
		tc.Time >= 0 && tc.Inc >= 0 &&
		(tc.Time > 0 || tc.Inc > 0)
}

// Engine stores necessary information to build and run an engine.
type Engine struct {
	Name       string   // name; optional
	Repository string   // git repository; required
	Branch     string   // git branch; required
	Commit     string   // git commit; required
	InitStr    []string // initial commands to send
}

// Link returns a link to the engine's code.
// So far it assumes that the repository is a public link on bitbucket.
func (e *Engine) Link() string {
	if strings.HasPrefix(e.Repository, "https://bitbucket.org/") {
		return strings.TrimSuffix(e.Repository, ".git") + "/commits/branch/" + e.Branch
	}
	return e.Repository
}

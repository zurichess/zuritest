// SPRT math is taken from cutechess. I don't claim to understand it.
// See https://github.com/cutechess/cutechess/blob/8eab971ed7ef73809efb62f10c77ae3e570db601/projects/lib/src/sprt.cpp

package common

import (
	"math"
)

const (
	σ95     = 1.959964
	σ99     = 2.575829
	games95 = 4000
	games99 = 4000 * (σ95 / σ99) * (σ95 / σ99)
)

type bayesElo struct {
	bayesElo float64
	drawElo  float64
}

func MakeBayesElo(s Score) bayesElo {
	return bayesElo{
		bayesElo: 200 * math.Log10(s.PWin()/s.PLoss()*(1-s.PLoss())/(1-s.PWin())),
		drawElo:  200 * math.Log10((1-s.PLoss())/s.PLoss()*(1-s.PWin())/s.PWin()),
	}
}

func (be *bayesElo) PWin() float64 {
	return 1 / (1 + math.Pow(10, (be.drawElo-be.bayesElo)/400))
}

func (be *bayesElo) PLoss() float64 {
	return 1 / (1 + math.Pow(10, (be.drawElo+be.bayesElo)/400))
}

func (be *bayesElo) PDraw() float64 {
	return 1 - be.PWin() - be.PLoss()
}

// Score of test vs base.
type Score struct {
	Win, Loss, Draw int
}

// Games returns the total number of games played.
func (s *Score) Games() int {
	return s.Win + s.Loss + s.Draw
}

// Score returns the score of test.
func (s *Score) Score() float64 {
	return float64(2*s.Win+s.Loss) / float64(2*s.Games())
}

// PWin returns the probability to win.
func (s *Score) PWin() float64 {
	return float64(s.Win) / float64(s.Games())
}

// PLoss returns the probability to lose.
func (s *Score) PLoss() float64 {
	return float64(s.Loss) / float64(s.Games())
}

// PDraw returns the probability to draw.
func (s *Score) PDraw() float64 {
	return float64(s.Draw) / float64(s.Games())
}

// LOS computes likelihood of superiority.
// See: https://chessprogramming.wikispaces.com/Match+Statistics
func (s *Score) LOS() float64 {
	return 0.5 * (1 + math.Erf(float64(s.Win-s.Loss)/math.Sqrt(2*float64(s.Win+s.Loss))))
}

// elo returns elo given probability of win.
func elo(x float64) float64 {
	return -400 * math.Log10(1/x-1)
}

// ELO returns elo and elo margin, and likelihood of superiority.
func (s *Score) ELO() (float64, float64) {
	n := float64(s.Games())
	w := float64(s.Win) / n
	l := float64(s.Loss) / n
	d := float64(s.Draw) / n

	μ := w + d*0.5
	σ := math.Sqrt((w*(1-μ)*(1-μ) + l*(0-μ)*(0-μ) + d*(0.5-μ)*(0.5-μ)) / (n - 1))

	c := 1.95966
	return elo(μ), (elo(μ+c*σ) - elo(μ-c*σ)) / 2
}

const (
	Continue int = iota
	AcceptH0
	AcceptH1
)

type SPRTStatus struct {
	Decision   int     // one of Continue, AcceptH0 or AcceptH1.
	LLR        float64 // log likelihood ratio
	LowerBound float64
	UpperBound float64
}

type SPRT struct {
	Elo0, Elo1  float64
	Alpha, Beta float64
}

// IsValid returns true if sprt is valid.
func (sprt *SPRT) IsValid() bool {
	return (sprt.Elo0 != sprt.Elo1) &&
		(sprt.Alpha > 0 && sprt.Alpha < 1) &&
		(sprt.Beta > 0 && sprt.Beta < 1)
}

// Status does the hypothesis thesis.
func (sprt *SPRT) Status(s Score) SPRTStatus {
	b := MakeBayesElo(s)
	b0 := bayesElo{sprt.Elo0, b.drawElo}
	b1 := bayesElo{sprt.Elo1, b.drawElo}

	var status SPRTStatus
	status.LLR = float64(s.Win) * (math.Log(b1.PWin()) - math.Log(b0.PWin()))
	status.LLR += float64(s.Loss) * (math.Log(b1.PLoss()) - math.Log(b0.PLoss()))
	status.LLR += float64(s.Draw) * (math.Log(b1.PDraw()) - math.Log(b0.PDraw()))

	status.LowerBound = math.Log(sprt.Beta / (1 - sprt.Alpha))
	status.UpperBound = math.Log((1 - sprt.Beta) / sprt.Alpha)

	if status.LLR > status.UpperBound {
		status.Decision = AcceptH1
	} else if status.LLR < status.LowerBound {
		status.Decision = AcceptH0
	} else {
		status.Decision = Continue
	}

	return status
}

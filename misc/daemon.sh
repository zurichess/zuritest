#!/bin/bash
# This script downloads and starts the latest version of the zuritest daemon.

set -x

killall -9 daemon
killall -9 zurichess

SERVICE=... # TODO

export GOPATH=$HOME/workspace
export PATH=$HOME/cutechess/projects/cli:$PATH

# Prepare the reference repository.
rm -rf $HOME/zurichess.git
git clone --bare https://brtzsnr@bitbucket.org/zurichess/zurichess.git

# Clean the temporary directory.
rm -rf /tmp/zurichess-*

# Install and start the daemon.
cd `dirname $0`
rm -rf src/bitbucket.org/zurichess/zuritest
go get -u bitbucket.org/zurichess/zuritest/daemon


hardware=`cat /proc/cpuinfo | grep Hardware | cut -f2 -d' '`

if [ $hardware == "ODROID-U2/U3" ]; then

tmux new-window $GOPATH/bin/daemon \
         --openings $HOME/workspace/bin/2moves_v1.pgn \
         --port 7070 \
         --reference $HOME/zurichess.git \
         --service http://spheric-vine-836.appspot.com/_rpc \
         --bench 236521

elif [ $hardware == "ODROID-C2" ]; then 

tmux new-window $GOPATH/bin/daemon \
         --openings $HOME/workspace/bin/2moves_v1.pgn \
         --port 7070 \
         --reference $HOME/zurichess.git \
         --service $SERVICE \
         --bench 247690

elif [ $hardware == "ODROID-XU3" ]; then

tmux new-window taskset -c 0,1,2,3 $GOPATH/bin/daemon \
         --openings $HOME/workspace/bin/2moves_v1.pgn \
         --port 7070 \
         --reference $HOME/zurichess.git \
         --service $SERVICE \
         --bench 145872

tmux new-window taskset -c 4,5,6,7 $GOPATH/bin/daemon \
         --openings $HOME/workspace/bin/2moves_v1.pgn \
         --port 7070 \
         --reference $HOME/zurichess.git \
         --service $SERVICE \
         --bench 353550

fi

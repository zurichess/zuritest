// zurieval is a tool to send the current zurichess branch for eval.
// This tool will take two branches of zurichess repository, base (defaults to master)
// and test (defaults to current branch), and schedules many games
// between them at one of the time controls and one of the SPRT tests.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"

	"bitbucket.org/zurichess/zuritest/common"
)

const (
	// A known good ancestor. Make sure that the commit can be go install.
	ancestor = "4775853cc58bf15d65cf3ad79ab75a9fb734ac04"
)

var (
	defaultService = "http://localhost:8080/_rpc"
)

var (
	service     = flag.String("service", defaultService, "JSON-RPC server address")
	tc          = flag.String("tc", "", "time control. can be one of short, long or huge")
	sprt        = flag.String("sprt", "standard", "sprt type. can be one of standard, tweak or no-regression")
	games       = flag.Int("games", 99999, "number of games")
	repository  = flag.String("repository", "https://bitbucket.org/brtzsnr/zurichess.git", "repository containing branches to test")
	baseBranch  = flag.String("base", "master", "base branch")
	testBranch  = flag.String("test", "", "test branch. if missing, current branch")
	fastForward = flag.Bool("fast-forward", true, "true if test much be a descendant of base")
)

// git executes a git commit capturing the output
func git(args ...string) (string, error) {
	proc := exec.Command("git", args...)
	proc.Stderr = os.Stderr
	output, err := proc.Output()
	if err != nil {
		return "", fmt.Errorf("git %s: %v", args[0], err)
	}
	return strings.TrimSpace(string(output)), nil
}

func getCurrentBranch() (string, error) {
	return git("rev-parse", "--abbrev-ref", "HEAD")
}

// getRefCommit get ref's commit.
// ref should be 'refs/heads/branch' for local branch
// or 'refs/remotes/origin/branch' for remote branch.
func getRefCommit(ref string) (string, error) {
	return git("show-ref", "--hash", ref)
}

// isAncestor returns true if commit1 is an ancestor of commit2.
func isAncestor(commit1, commit2 string) bool {
	_, err := git("merge-base", "--is-ancestor", commit1, commit2)
	return err == nil
}

// octopus returns first common ancestor.
func octopus(commit1, commit2 string) (string, error) {
	return git("merge-base", "--octopus", commit1, commit2)
}

func getDescription(base, test string) (string, error) {
	common, err := octopus(base, test)
	if err != nil {
		return "", err
	}

	// Put a + before every commit in test, but not in base.
	testDesc, err := git("log", "--oneline", common+"..."+test)
	if err != nil {
		return "", err
	}
	testDesc = strings.Replace(testDesc, "\n", "\n+", -1)

	// Put a - before every commit in base, but not in test.
	baseDesc, err := git("log", "--oneline", common+"..."+base)
	if err != nil {
		return "", err
	}
	baseDesc = strings.Replace(baseDesc, "\n", "\n-", -1)

	desc := ""
	if testDesc != "" {
		desc += "+" + testDesc + "\n"
	}
	if baseDesc != "" {
		desc += "-" + baseDesc + "\n"
	}

	return desc, nil
}

// getCommit returns the last commit.
// It makes sure that the last commit is pushed to origin.
func getCommit(branch string) (string, error) {
	commit, err := getRefCommit("refs/heads/" + branch)
	if err != nil {
		log.Print("Is this a git repository?")
		return "", err
	}
	if !isAncestor(ancestor, commit) {
		return "", fmt.Errorf("%s (%s) must be a descendant of %s",
			commit[:6], branch, ancestor[:6])
	}

	origin, err := getRefCommit("refs/remotes/origin/" + branch)
	if err != nil {
		log.Printf("Branch %s is not pushed to origin", branch)
		log.Printf("    git push --set-upstream origin %s", branch)
		return "", err
	}
	if commit != origin {
		log.Printf("Commit %s is not pushed to origin", commit[:6])
		log.Printf("    git push --force %s", branch)
		return "", fmt.Errorf("%s (%s) must be a descendant of %s",
			commit[:6], branch, origin[:6])
	}

	return commit, nil
}

func stringToTimeControl(str string) (common.TimeControl, error) {
	switch str {
	case "short":
		return common.TimeControl{
			Moves: 40,
			Time:  15 * time.Second,
			Inc:   150 * time.Millisecond,
		}, nil

	case "long":
		return common.TimeControl{
			Moves: 40,
			Time:  60 * time.Second,
			Inc:   600 * time.Millisecond,
		}, nil

	case "huge":
		return common.TimeControl{
			Moves: 40,
			Time:  240 * time.Second,
			Inc:   2400 * time.Millisecond,
		}, nil

	default:
		return common.TimeControl{}, fmt.Errorf("Must be one of short, long or huge")
	}
}

// stringToSPRT converts a SPRT type to a SPRT object.
//
// Elo values are the same used by fishtest. Alpha and Beta values
// are chosen to optimize the number of tests when the ratio of
// good patches to all patches is about 15%.
func stringToSPRT(str string) (common.SPRT, error) {
	switch str {
	case "standard":
		return common.SPRT{
			Elo0:  0,
			Elo1:  6,
			Alpha: 0.03,
			Beta:  0.15,
		}, nil
	case "tweak":
		return common.SPRT{
			Elo0:  0,
			Elo1:  4,
			Alpha: 0.03,
			Beta:  0.15,
		}, nil
	case "no-regression":
		return common.SPRT{
			Elo0:  -3,
			Elo1:  1,
			Alpha: 0.05,
			Beta:  0.15,
		}, nil
	default:
		return common.SPRT{}, fmt.Errorf("Must be one of standard, tweak or no-regression.")
	}
}

func main() {
	flag.Parse()
	log.SetFlags(log.Ltime | log.Lshortfile)

	if *testBranch == "" {
		var err error
		if *testBranch, err = getCurrentBranch(); err != nil {
			log.Fatal(err)
		}
	}

	timeControl, err := stringToTimeControl(*tc)
	if err != nil {
		log.Print("Invalid value for flag --tc")
		log.Fatal(err)
	}
	sprt, err := stringToSPRT(*sprt)
	if err != nil {
		log.Print("Invalid value for flag --sprt")
		log.Fatal(err)
	}

	// Get the test and base commits.
	testCommit, err := getCommit(*testBranch)
	if err != nil {
		log.Fatal(err)
	}
	baseCommit, err := getCommit(*baseBranch)
	if err != nil {
		log.Fatal(err)
	}
	if *fastForward && !isAncestor(baseCommit, testCommit) {
		log.Fatalf("Test branch %s [%s] must be descendent of base branch %s [%s]",
			*testBranch, testCommit[:6], *baseBranch, baseCommit[:6])
	}

	description, err := getDescription(baseCommit, testCommit)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%s [%s] vs %s [%s]\n", *baseBranch, baseCommit[:6], *testBranch, testCommit[:6])
	log.Printf("with description\n%s", description)

	client := &common.MatchServiceClient{Address: *service}
	req := &common.CreateRequest{
		Description: description,
		GamesToPlay: *games,
		TimeControl: timeControl,
		BaseEngine: common.Engine{
			Name:       *baseBranch,
			Repository: *repository,
			Branch:     *baseBranch,
			Commit:     baseCommit,
		},
		TestEngine: common.Engine{
			Name:       *testBranch,
			Repository: *repository,
			Branch:     *testBranch,
			Commit:     testCommit,
		},
		SPRT: sprt,
	}

	if resp, err := client.Create(req); err != nil {
		log.Fatalf("Could not create match: %v", err)
	} else {
		log.Printf("Requested %d games at %v", *games, req.TimeControl)
		log.Printf("Got game id %s", resp.Id)
	}
}

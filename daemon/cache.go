// cache.go implements a cache of engine binaries.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"

	"bitbucket.org/zurichess/zuritest/common"
)

var (
	cacheSize = 8
)

type entry struct {
	binary string
	stamp  int
}

type cache struct {
	stamp     int
	tmpdir    string
	reference string
	engines   map[string]*entry // maps commit to binary
}

// createEngine creates a new engine, reusing from cache if possible.
//
// TODO: a bug is possible here because we can delete an engine in use.
func (c *cache) createEngine(engine *common.Engine) (string, error) {
	c.stamp++
	if c.engines == nil {
		c.engines = make(map[string]*entry)
	}
	if entry, has := c.engines[engine.Commit]; has {
		if _, err := os.Stat(entry.binary); err == nil {
			log.Println("Reusing", entry.binary)
			entry.stamp = c.stamp
			return entry.binary, nil
		}

	}

	binary, err := c.buildEngine(engine)
	if err != nil {
		return "", err
	}

	// Evict something from cache if it's full.
	if len(c.engines) == cacheSize {
		minStamp := c.stamp
		for _, v := range c.engines {
			if v.stamp < minStamp {
				minStamp = v.stamp
			}
		}
		for k, v := range c.engines {
			if v.stamp == minStamp {
				// Drop bin/zurichess
				log.Println("Evicting", v.binary)
				os.RemoveAll(path.Base(path.Base(v.binary)))
				delete(c.engines, k)
				break
			}
		}
	}

	// Insert the engine in the cache.
	c.engines[engine.Commit] = &entry{
		binary: binary,
		stamp:  c.stamp,
	}
	return binary, nil
}

// buildEngine builds an engine.
// Returns binary path or an error.
func (c *cache) buildEngine(engine *common.Engine) (string, error) {
	// Create a temporary directory where to build the engine.
	if len(engine.Commit) != 40 {
		return "", fmt.Errorf("Invalid SHA1 commit")
	}
	gopath, err := ioutil.TempDir(c.tmpdir, "zurichess-"+engine.Commit[:6]+"-")
	if err != nil {
		return "", err
	}

	zurichess := path.Join(gopath, "src/bitbucket.org/zurichess/zurichess")

	// Clone git repository. If available, then use a reference repository
	// to decrease network traffic and space used.
	args := []string{"clone"}
	if c.reference != "" {
		args = append(args, "--reference", *reference)
	}
	args = append(args, "--branch", engine.Branch, engine.Repository, zurichess)
	clone := exec.Command("git", args...)
	clone.Stderr = os.Stderr
	if err := clone.Run(); err != nil {
		log.Println(clone.Args)
		return "", fmt.Errorf("git clone: %v", err)
	}

	// Checkout at engine's commit.
	// Commit should be a parent of branch.
	checkout := exec.Command("git", "-C", zurichess, "checkout", "--quiet", engine.Commit)
	checkout.Stderr = os.Stderr
	if err := checkout.Run(); err != nil {
		log.Println(clone.Args)
		log.Println(checkout.Args)
		return "", fmt.Errorf("git checkout: %v", err)
	}

	// Compile and install zurichess engine.
	install := exec.Command("go", "install", "-gcflags=-B", "bitbucket.org/zurichess/zurichess/zurichess")
	install.Stderr = os.Stderr
	install.Env = []string{"GOPATH=" + gopath}
	install.Dir = gopath
	if err := install.Run(); err != nil {
		return "", fmt.Errorf("go install: %v", err)
	}

	// Cleanup source and packages
	os.RemoveAll(path.Join(gopath, "src"))
	os.RemoveAll(path.Join(gopath, "pkg"))

	binary := path.Join(gopath, "bin/zurichess")
	log.Println("Built", binary)
	return binary, nil
}

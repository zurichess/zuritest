// +build appengine

package appengine

import (
	"appengine"
	"appengine/datastore"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"bitbucket.org/zurichess/zuritest/common"
)

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

func init() {
	rand.Seed(time.Now().Unix())
}

func (ms *MatchService) GetWork(r *http.Request,
	req *common.GetWorkRequest,
	resp *common.GetWorkResponse) error {
	c := appengine.NewContext(r)

	t := datastore.NewQuery(KeyKind).
		Filter("State = ", Running).
		Order("Birth").
		Limit(10).Run(c)
	num := 0

	for {
		var match Match
		key, err := t.Next(&match)
		if err == datastore.Done {
			return nil
		}
		if err != nil {
			return fmt.Errorf("fetching next match: %v", err)
		}
		if match.Score.Games() >= match.GamesToPlay {
			continue
		}

		num++
		if rand.Intn(num) == 0 {
			// Pick a random match out of the available matches.
			*resp = common.GetWorkResponse{
				Id:          key.Encode(),
				GamesToPlay: min(GamesPerRound, match.GamesToPlay-match.Score.Games()),
				TimeControl: match.TimeControl,
				BaseEngine:  match.BaseEngine,
				TestEngine:  match.TestEngine,
			}
		}
	}
}

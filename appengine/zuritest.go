// +build appengine

package appengine

import (
	"appengine"
	"appengine/datastore"
	"appengine/user"
	"net/http"
	"time"

	"bitbucket.org/zurichess/zuritest/common"
	"github.com/gorilla/rpc/v2"
	"github.com/gorilla/rpc/v2/json"
)

type MatchService struct {
}

// matchEntry encapsulates data to be presented.
type matchEntry struct {
	Id        string
	ELO       float64
	ELOMargin float64

	Match  Match
	Status common.SPRTStatus
}

func stringToMatchState(s string) MatchState {
	switch s {
	case "Created":
		return Created
	case "Running":
		return Running
	case "Stopped":
		return Stopped
	case "Finished":
		return Finished
	default:
		return Running
	}
}

// list lists matches.
func list(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-cache")
	c := appengine.NewContext(r)

	if err := r.ParseForm(); err != nil {
		c.Errorf("Could not parse form: %v", err)
		http.Error(w, "Could not parse form.", http.StatusBadRequest)
		return
	}

	t := datastore.NewQuery(KeyKind).
		Filter("State =", stringToMatchState(r.Form.Get("state"))).
		Order("-Birth").Limit(100).Run(c)

	var matches []matchEntry
	for {
		var match Match
		key, err := t.Next(&match)
		if err == datastore.Done {
			break
		} else if err != nil {
			c.Errorf("Reading matches: %v", err)
			return
		}

		if !match.SPRT.IsValid() {
			// If SPRT parameters are invalid, than this match was
			// created before SPRT was added to zurieval.
			match.SPRT = common.SPRT{0, 6, 0.03, 0.15}

		}

		entry := matchEntry{
			Id:     key.Encode(),
			Match:  match,
			Status: match.SPRT.Status(match.Score),
		}

		entry.ELO, entry.ELOMargin = match.Score.ELO()
		matches = append(matches, entry)
	}

	if err := rootTmpl.ExecuteTemplate(w, "", matches); err != nil {
		c.Errorf("list: could not render template: %v", err)
		return
	}
}

func updateMatchState(w http.ResponseWriter, r *http.Request, state MatchState) {
	w.Header().Add("Cache-Control", "no-cache")
	c := appengine.NewContext(r)
	if !user.IsAdmin(c) {
		http.Error(w, "Not an admin.", http.StatusForbidden)
		return
	}
	if err := r.ParseForm(); err != nil {
		http.Error(w, "Cannot parse form.", http.StatusBadRequest)
		return
	}
	matchKey, err := datastore.DecodeKey(r.Form.Get("id"))
	if err != nil {
		http.Error(w, "Bad match id.", http.StatusBadRequest)
		return
	}

	err = datastore.RunInTransaction(c, func(c appengine.Context) error {
		m := &Match{}
		if err := datastore.Get(c, matchKey, m); err != nil {
			return err
		}
		m.Birth = time.Now()
		m.State = state
		_, err = datastore.Put(c, matchKey, m)
		return err
	}, nil)
	if err != nil {
		c.Errorf("updateMatchState: could not update match: %v", err)
		http.Error(w, "Bad match id.", http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/", 307)
}

func stop(w http.ResponseWriter, r *http.Request) {
	updateMatchState(w, r, Stopped)
}

func resume(w http.ResponseWriter, r *http.Request) {
	updateMatchState(w, r, Running)
}

func init() {
	http.Handle("/", http.RedirectHandler("/list?state=Running", http.StatusMovedPermanently))
	http.HandleFunc("/list", list)
	http.HandleFunc("/admin/stop", stop)
	http.HandleFunc("/admin/resume", resume)

	s := rpc.NewServer()
	s.RegisterCodec(json.NewCodec(), "application/json")
	s.RegisterService(new(MatchService), "")
	http.Handle("/_rpc", s)
}

// +build appengine

package appengine

import "html/template"

var rootTmpl = template.Must(template.New("").Parse(`
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>zuritest</title>
<style type="text/css">
  .TFtable {
    border-collapse:collapse; 
  }
  .TFtable th {
    text-align: left;
    padding:7px; border:#4e95f4 1px solid;
  }
  .TFtable td { 
    nowrap;
    padding:7px; border:#4e95f4 1px solid;
  }
  /* provide some minimal visual accomodation for IE8 and below */
  .TFtable tr {
    background: #b8d1f3;
  }
  /*  Define the background color for all the ODD background rows  */
  .TFtable tr:nth-child(odd) { 
    background: White;
  }
  /*  Define the background color for all the EVEN background rows  */
  .TFtable tr:nth-child(even) {
    background: AliceBlue;
  }
</style>
</head>
<body>
<p>
<a href="/list?state=Created">Created</a>
<a href="/list?state=Running">Running</a>
<a href="/list?state=Stopped">Stopped</a>
<a href="/list?state=Finished">Finished</a>
</p>

<table class="TFtable">
  <tr>
    <th>Test</th>
    <th>Base</th>
    <th>State</th>
    <th>SPRT</th>
    <th>Score</th>
    <th>Description</th>
  </tr>
  {{range .}}
  <tr>
    <td><a href="{{.Match.TestEngine.Link}}" title="{{.Match.TestEngine.Commit}}">{{.Match.TestEngine.Name}}</a></td>
    <td><a href="{{.Match.BaseEngine.Link}}" title="{{.Match.BaseEngine.Commit}}">{{.Match.BaseEngine.Name}}</a></td>
    <td>
        <form action={{if ne .Match.State 1}}"/admin/resume"{{else}}"/admin/stop"{{end}}>
            <input type="hidden" name="id" value="{{.Id}}">
            <button type="submit" style="border:1px solid black; background-color: transparent;">{{if ne .Match.State 1}}&#9654;{{else}}&#9632;{{end}}</button>&nbsp;{{.Match.State}}
        </form>
    </td>
    <td valign="top"{{if eq .Status.Decision 2}} style="background-color:LightGreen"{{end}}{{if eq .Status.Decision 1}} style="background-color:OrangeRed"{{end}}>
        <pre style="margin-top:0; margin-bottom:0;">
 Elo0:{{printf "% 4.2f" .Match.SPRT.Elo0}} Elo1:{{printf "% 4.2f" .Match.SPRT.Elo1}}
Alpha:{{printf "% 4.2f" .Match.SPRT.Alpha}} Beta:{{printf "% 4.2f" .Match.SPRT.Beta}}
LLR:{{printf "% 4.2f" .Status.LLR}} [{{printf "%+.2f" .Status.LowerBound}}:{{printf "%+.2f" .Status.UpperBound}}]</pre></td>
    <td valign="top"{{if eq .Status.Decision 2}} style="background-color:LightGreen"{{end}}{{if eq .Status.Decision 1}} style="background-color:OrangeRed"{{end}}>
        <pre style="margin-top:0; margin-bottom:0;">{{.Match.Score.Games}} @ {{.Match.TimeControl.String}}
{{.Match.Score.Win}} - {{.Match.Score.Loss}} - {{.Match.Score.Draw}}
ELO {{printf "%.2f" .ELO}}±{{printf "%.2f" .ELOMargin}}</pre></td>
    <td valign="top"><pre style="margin-top:0; margin-bottom:0;">{{.Match.Description}}</pre></td>
  </tr>
  {{end}}
</table> </body> </html>
`))

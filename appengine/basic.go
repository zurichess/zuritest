// +build appengine

package appengine

import (
	"time"

	"bitbucket.org/zurichess/zuritest/common"
)

const (
	KeyKind       = "Data"
	GamesPerRound = 100
)

// MatchState represents the state of the game.
type MatchState int

const (
	Created MatchState = iota
	Running
	Stopped
	Finished
)

type Match struct {
	Birth time.Time
	State MatchState

	Description string             `datastore:",noindex"`
	GamesToPlay int                `datastore:",noindex"`
	TimeControl common.TimeControl `datastore:",noindex"`
	BaseEngine  common.Engine      `datastore:",noindex"`
	TestEngine  common.Engine      `datastore:",noindex"`
	SPRT        common.SPRT        `datastore:",noindex"`
	Score       common.Score       `datastore:",noindex"`
}

// HasFinished returns true if a decision has been reached.
func (m *Match) HasFinished() bool {
	return m.SPRT.Status(m.Score).Decision != common.Continue
}

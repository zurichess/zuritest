// +build appengine

package appengine

import (
	"appengine"
	"appengine/datastore"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/zurichess/zuritest/common"
)

func (ms *MatchService) Create(r *http.Request,
	req *common.CreateRequest,
	resp *common.CreateResponse) error {
	c := appengine.NewContext(r)

	if len(req.BaseEngine.Commit) != 40 || req.BaseEngine.Branch == "" || req.BaseEngine.Repository == "" {
		return fmt.Errorf("invalid base engine")
	}
	if len(req.TestEngine.Commit) != 40 || req.TestEngine.Branch == "" || req.TestEngine.Repository == "" {
		return fmt.Errorf("invalid test engine")
	}
	if req.GamesToPlay <= 0 {
		return fmt.Errorf("must play at least 1 game")
	}
	if req.Description == "" {
		return fmt.Errorf("must provide a description")
	}
	if !req.TimeControl.IsValid() {
		return fmt.Errorf("invalid time control")
	}
	if !req.SPRT.IsValid() {
		return fmt.Errorf("invalid sprt parameters")
	}

	match := &Match{
		Birth: time.Now(),
		State: Created,

		Description: req.Description,
		GamesToPlay: req.GamesToPlay,
		TimeControl: req.TimeControl,
		BaseEngine:  req.BaseEngine,
		TestEngine:  req.TestEngine,
		SPRT:        req.SPRT,
	}

	// Give names based on branch.
	if match.BaseEngine.Name == "" {
		match.BaseEngine.Name = match.BaseEngine.Branch
	}
	if match.TestEngine.Name == "" {
		match.TestEngine.Name = match.TestEngine.Branch
	}

	key, err := datastore.Put(c, datastore.NewIncompleteKey(c, KeyKind, nil), match)
	if err != nil {
		return err
	}
	resp.Id = key.Encode()
	return nil
}

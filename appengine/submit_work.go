// +build appengine

package appengine

import (
	"appengine"
	"appengine/datastore"
	"net/http"
	"time"

	"bitbucket.org/zurichess/zuritest/common"
)

func (ms *MatchService) SubmitWork(r *http.Request, req *common.SubmitWorkRequest, resp *common.SubmitWorkResponse) error {
	matchKey, err := datastore.DecodeKey(req.Id)
	if err != nil {
		return err
	}

	c := appengine.NewContext(r)
	return datastore.RunInTransaction(c, func(c appengine.Context) error {
		m := &Match{}
		if err := datastore.Get(c, matchKey, m); err != nil {
			return err
		}

		m.Score.Win += req.Score.Win
		m.Score.Loss += req.Score.Loss
		m.Score.Draw += req.Score.Draw

		if m.HasFinished() {
			m.Birth = time.Now()
			m.State = Finished
		} else if m.Score.Games() >= m.GamesToPlay {
			m.Birth = time.Now()
			m.State = Stopped
		}

		_, err = datastore.Put(c, matchKey, m)
		return err
	}, nil)
}
